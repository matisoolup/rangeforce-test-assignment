module.exports = {
  css: {
    loaderOptions: {
      less: {
        additionalData: `
    @import "@/assets/styles/global-styles.less";
    `,
      },
    },
  },
};
