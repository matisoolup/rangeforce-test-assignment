import { createRouter, createWebHistory } from "vue-router";
import Search from "../views/Search";
import Bookmarks from "../views/Bookmarks";
import NotFound from "../views/NotFound";

const routes = [
  {
    path: "/",
    name: "Search",
    component: Search,
  },
  {
    path: "/bookmarks",
    name: "Bookmarks",
    component: Bookmarks,
  },
  {
    path: "/:notFound(.*)",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
