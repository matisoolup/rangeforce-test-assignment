import BookmarksActions from "./actions";
import BookmarksMutations from "./mutations";
import BookmarksGetters from "./getters";

export default {
  namespaced: true,
  state() {
    return {
      bookmarks: [],
    };
  },
  mutations: BookmarksMutations,
  actions: BookmarksActions,
  getters: BookmarksGetters,
};
