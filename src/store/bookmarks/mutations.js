export default {
  addBookmark(state, payload) {
    let payloadBookmark = payload.value;
    let bookmarkExists = state.bookmarks.some(
      (repo) => repo.id === payloadBookmark.id
    );

    if (!bookmarkExists) {
      state.bookmarks.push(payloadBookmark);
    }
  },
  deleteBookmark(state, payload) {
    let payloadBookmark = payload.value;
    state.bookmarks = state.bookmarks.filter(
      (bookmark) => bookmark.id !== payloadBookmark.id
    );
  },
};
