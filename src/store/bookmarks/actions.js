export default {
  addBookmark(context, payload) {
    context.commit("addBookmark", payload);
    updateSearchResults(context, payload, true);
  },
  deleteBookmark(context, payload) {
    context.commit("deleteBookmark", payload);
    updateSearchResults(context, payload, false);
  },
};

const updateSearchResults = (context, payload, isBookmarked) => {
  let searchResults = context.rootState.searchResults.searchResults;
  let repo = payload.value;

  let foundIndex = searchResults.findIndex((result) => result.id == repo.id);
  context.rootState.searchResults.searchResults[
    foundIndex
  ].bookmarked = isBookmarked;
};
