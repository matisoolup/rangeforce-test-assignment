import searchResultsActions from "./actions";
import searchResultsMutations from "./mutations";
import searchResultsGetters from "./getters";

export default {
  namespaced: true,
  state: {
    searchResults: [],
    totalResults: 0,
  },
  mutations: searchResultsMutations,
  actions: searchResultsActions,
  getters: searchResultsGetters,
};
