import axios from "axios";

axios.defaults.baseURL = "https://api.github.com/";

export default {
  loadResults(context, payload) {
    axios
      .get("search/repositories", {
        params: {
          q: payload.value.searchTerm,
          per_page: payload.value.perPage,
          page: payload.value.page,
        },
      })
      .then((result) => {
        context.commit("saveResults", result.data);
        updateSearchResults(context, result.data.items);
      })
      .catch((error) => {
        throw new Error(`API ${error}`);
      });
  },
};

const updateSearchResults = (context, searchResults) => {
  let bookmarks = context.rootState.bookmarks.bookmarks;
  bookmarks.forEach((bookmark) => {
    let bookmarkIndex = searchResults.findIndex(
      (result) => result.id == bookmark.id
    );
    if (bookmarkIndex > -1) {
      context.rootState.searchResults.searchResults[
        bookmarkIndex
      ].bookmarked = true;
    }
  });
};
