export default {
  searchResults(state) {
    return state.searchResults;
  },
  totalResults(state) {
    return state.totalResults;
  },
};
