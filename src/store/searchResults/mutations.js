export default {
  saveResults(state, searchResults) {
    state.searchResults = searchResults.items;
    state.totalResults = searchResults.total_count;
  },
};
