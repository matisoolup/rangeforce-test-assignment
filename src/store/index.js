import { createStore } from "vuex";
import VuexPersistence from "vuex-persist";
import searchResultsModule from "./searchResults/index";
import bookmarksModule from "./bookmarks/index";

const vuexPersist = new VuexPersistence({
  storage: window.localStorage,
  reducer: (state) => ({
    bookmarks: state.bookmarks,
  }),
});

export default createStore({
  modules: {
    searchResults: searchResultsModule,
    bookmarks: bookmarksModule,
  },
  plugins: [vuexPersist.plugin],
});
